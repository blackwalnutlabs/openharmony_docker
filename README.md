# OpenHarmony Docker 编译

- [OpenHarmony Docker 编译介绍](#OpenHarmony_Docker介绍)
- [平台支持](#平台支持)
- [OpenHarmony_Docker使用](#OpenHarmony_Docker使用)
    - [获取镜像](#获取镜像)
    - [编译](#编译)
        - [Hi3851](#Hi3851)
        - [Hi3516](#Hi3516)
        - [Hi3518](#Hi3518)
- [许可证](#许可证)

## OpenHarmony_Docker介绍

`OpenHarmony` 是开放原子开源基金会（OpenAtom Foundation）旗下开源项目，定位是一款面向全场景的开源分布式操作系统。
`OpenHarmony` 在传统的单设备系统能力的基础上，创造性地提出了基于同一套系统能力、适配多种终端形态的理念，支持多种终端设备上运行，现在有第一个版本支持128K-128M设备上运行。

`OpenHarmony_Docker` 则是针对 `OpenHarmony` 项目在编译时面临的环境不统一的情况而使用 `Docker` 来对其进行容器化编译的一个项目。

`Docker` 是一个开源的应用容器引擎，让开发者可以打包他们的应用以及依赖包到一个可移植的容器中,然后发布到任何流行的Linux机器或Windows 机器上,也可以实现虚拟化,容器是完全使用沙箱机制,相互之间不会有任何接口。

## 平台支持

| 硬件平台| 状态   |
| :----- | :--- |
| Hi3861 |  ✔️   |
| Hi3516 |  ✔️   |
| Hi3518 |  ✔️   |

## OpenHarmony_Docker使用

### 获取镜像

| 硬件平台| 镜像                                    |
| :----- | :------------------------------------- |
| Hi3861 |  misakajimmy/openharmony_hi3861:latest |
| Hi3516 |  misakajimmy/openharmony_hi3516:latest |
| Hi3518 |  misakajimmy/openharmony_hi3518:latest |

```shell
docker image pull 镜像地址
```

### 编译

```shell
cd 项目目录
docker run -it -v $(pwd):/home  misakajimmy/harmony-compile:latest python build.py 
```

#### Hi3851

详情烧写与源码获取可见 [Hi3861开发板第一个示例程序](https://gitee.com/openharmony/docs/blob/master/quick-start/Hi3861开发板第一个示例程序.md)

```shell
cd 项目目录
docker run -it -v $(pwd):/home  misakajimmy/openharmony-hi3861:latest python build.py wifiiot
```

#### Hi3516

详情烧写与源码获取可见 [开发Hi3516第一个应用程序示例](https://gitee.com/openharmony/docs/blob/master/quick-start/开发Hi3516第一个应用程序示例.md)

```shell
cd 项目目录
docker run -it -v $(pwd):/home  misakajimmy/openharmony-hi3516:latest python build.py ipcamera_hi3516dv300
```

#### Hi3518

详情烧写与源码获取可见 [开发Hi3518第一个示例程序](https://gitee.com/openharmony/docs/blob/master/quick-start/开发Hi3518第一个示例程序.md)

```shell
cd 项目目录
docker run -it -v $(pwd):/home  misakajimmy/openharmony-hi3518:latest python build.py ipcamera_hi3518ev300
```


## 许可证

[Apache License 2.0](LICENSE)